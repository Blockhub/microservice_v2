import os
from concurrent import futures
import time
from sklearn.externals import joblib

import model.reconmendation_engine as re
import grpc

import supplier_pb2
import supplier_pb2_grpc

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class SupplierPredictor(supplier_pb2_grpc.SupplierPredictorServicer):
    _model = None

    @classmethod
    def PredictSuppliers(self, request, context):

        print('Loading Recommendation Model')
        supllier_id = request.supllier_id

        result = re.Run().find_movies(supllier_id)
        if len(result) < 5:
            for i in range(0, 5 - len(result)):
                result.append("")

        return supplier_pb2.SupplierPredictReply(customers1=result[0], customers2=result[1], customers3=result[2], customers4=result[3], customers5=result[4])


def serve():
    print('Start server for Product Micro-service on port 50054')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    supplier_pb2_grpc.add_SupplierPredictorServicer_to_server(SupplierPredictor(), server)
    server.add_insecure_port('[::]:50054')
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
