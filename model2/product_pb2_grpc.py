# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

import product_pb2 as product__pb2


class ProductPredictorStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.PredictProducts = channel.unary_unary(
        '/ml.ProductPredictor/PredictProducts',
        request_serializer=product__pb2.ProductPredictRequest.SerializeToString,
        response_deserializer=product__pb2.ProductPredictReply.FromString,
        )


class ProductPredictorServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def PredictProducts(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_ProductPredictorServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'PredictProducts': grpc.unary_unary_rpc_method_handler(
          servicer.PredictProducts,
          request_deserializer=product__pb2.ProductPredictRequest.FromString,
          response_serializer=product__pb2.ProductPredictReply.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'ml.ProductPredictor', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
