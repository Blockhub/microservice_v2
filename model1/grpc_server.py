import os
from concurrent import futures
import time
from pprint import pprint

from sklearn.externals import joblib

import grpc

import customer_pb2
import customer_pb2_grpc

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class CustomerPredictor(customer_pb2_grpc.CustomerPredictorServicer):
    _model = None

    @classmethod
    def get_or_create_model(cls):
        """
        Get or create iris classification model.
        """
        if cls._model is None:
            path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'model', 'customer_purchase_model.pickle')
            path1 = os.path.join(os.path.dirname(os.path.relpath(__file__)), 'model', 'customer_purchase_model.pickle')
            print('path: ' + path)
            print('path1: ' + path1)

            # path='/model/customer_purchase_model.pickle'
            cls._model = joblib.load(path)
        return cls._model

    def PredictCustomers(self, request, context):
        model = self.__class__.get_or_create_model()
        print('Loading Model')
        customer_age = request.customer_age
        customer_gender = request.customer_gender
        customer_income = request.customer_income
        customer_headOfhousehold = request.customer_headOfhousehold
        customer_noOfhousehold = request.customer_noOfhousehold
        customer_monthOfresidence = request.customer_monthOfresidence
        customer_t1_30 = request.customer_t1_30
        customer_t2_30 = request.customer_t2_30
        customer_t3_30 = request.customer_t3_30
        customer_t1_10 = request.customer_t1_10
        customer_t2_10 = request.customer_t2_10
        customer_t3_10 = request.customer_t3_10
        customer_t1_3 = request.customer_t1_3
        customer_t2_3 = request.customer_t2_3
        customer_t3_3 = request.customer_t3_3
        result = model.predict_proba([[customer_age, customer_gender, customer_income, customer_headOfhousehold, customer_noOfhousehold, customer_monthOfresidence, customer_t1_30, customer_t2_30, customer_t3_30, customer_t1_10, customer_t2_10, customer_t3_10, customer_t1_3, customer_t2_3, customer_t3_3]])
        print(result)
        #return customer_pb2.CustomerPredictReply(buy_type1=result[0][0], buy_type2=result[0][1], buy_type3=result[0][2],buy_type4=result[0][3] )
        return customer_pb2.CustomerPredictReply(buy_type1=result[0][0], buy_type2=result[0][1], buy_type3=result[0][2],buy_type4=result[0][3] )


def serve():
    print('Start server at Port 50052')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    customer_pb2_grpc.add_CustomerPredictorServicer_to_server(CustomerPredictor(), server)
    server.add_insecure_port('[::]:50052')
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
