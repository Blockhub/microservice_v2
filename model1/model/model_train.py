from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn.neural_network import MLPClassifier
from sklearn.externals import joblib
import pandas as pd
import numpy as np



df_train = pd.read_csv('data/data_training.csv')

Y = df_train['label']
X = df_train.iloc[:, 1:df_train.shape[1]-1]


clf = RandomForestClassifier(max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto',
                             max_leaf_nodes=None, min_impurity_decrease=0.0, min_impurity_split=None, bootstrap=True, oob_score=False,
                             n_jobs=1, random_state=None, verbose=0, warm_start=False, class_weight=None)

clf.fit(X, Y)

joblib.dump(clf, 'customer_purchase_model.pickle')