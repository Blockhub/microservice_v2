from flask import Flask, Response, request, render_template, redirect, url_for
from flask_cors import CORS, cross_origin
import sys
import json
import os

from google.protobuf.json_format import MessageToJson
from client_wrapper import ServiceClient

from model1 import customer_pb2 as customer_pb2
from model1 import customer_pb2_grpc

from model2 import product_pb2
from model2 import product_pb2_grpc

from model3 import supplier_pb2
from model3 import supplier_pb2_grpc


app = Flask(__name__)

grpc_url='localhost'
try:
    grpc_url=os.environ["grpcurl"]
except KeyError:
    pass

app.config['customers'] = ServiceClient(customer_pb2_grpc, 'CustomerPredictorStub', grpc_url, 50052)
app.config['products'] = ServiceClient(product_pb2_grpc, 'ProductPredictorStub', grpc_url, 50053)
app.config['supplier'] = ServiceClient(supplier_pb2_grpc, 'SupplierPredictorStub', grpc_url, 50054)




CORS(app)
@app.route('/')
def index():
    return render_template('ui/index.html')

@app.route('/customers/', methods=['POST'])
def customer_get():
    # {"age": 55, "gender": 0, "income": 1, "headOfhousehold": 0, "noOfhousehold": 2, "monthOfresidence": 50,
    #  "t1_30": 332, "t2_30": 23902, "t3_30": 10963, "t1_10": 133, "t2_10": 14473, "t3_10": 3586, "t1_3": 26,
    #  "t2_3": 6824, "t3_3": 1403}
    data = request.get_json()
    print(data['noOfhousehold'])
    model_data = customer_pb2.CustomerPredictRequest(
        customer_age=int(data['age']),
        customer_gender=int(data['gender']),
        customer_income=int(data['income']),
        customer_headOfhousehold=int(data['headOfhousehold']),
        customer_noOfhousehold=int(data['noOfhousehold']),
        customer_monthOfresidence=int(data['monthOfresidence']),
        customer_t1_30=int(data['t1_30']),
        customer_t2_30=int(data['t2_30']),
        customer_t3_30=int(data['t3_30']),
        customer_t1_10=int(data['t1_10']),
        customer_t2_10=int(data['t2_10']),
        customer_t3_10=int(data['t3_10']),
        customer_t1_3=int(data['t1_3']),
        customer_t2_3=int(data['t2_3']),
        customer_t3_3=int(data['t3_3'])
    )

    def get_customer():
        response = app.config['customers'].PredictCustomers(model_data)
        print(response)
        yield MessageToJson(response)

    return Response(get_customer(), content_type='application/json')




@app.route('/products/', methods=['POST'])
def product_get():
    # {"age": 55, "gender": 0, "income": 1, "headOfhousehold": 0, "noOfhousehold": 2, "monthOfresidence": 50,
    #  "t1_30": 332, "t2_30": 23902, "t3_30": 10963, "t1_10": 133, "t2_10": 14473, "t3_10": 3586, "t1_3": 26,
    #  "t2_3": 6824, "t3_3": 1403}
    data = request.get_json()

    model_data = product_pb2.ProductPredictRequest(
        customer_age=int(data['age']),
        customer_gender=int(data['gender']),
        customer_income=int(data['income']),
        customer_headOfhousehold=int(data['headOfhousehold']),
        customer_noOfhousehold=int(data['noOfhousehold']),
        customer_monthOfresidence=int(data['monthOfresidence']),
        customer_t1_30=int(data['t1_30']),
        customer_t2_30=int(data['t2_30']),
        customer_t3_30=int(data['t3_30']),
        customer_t1_10=int(data['t1_10']),
        customer_t2_10=int(data['t2_10']),
        customer_t3_10=int(data['t3_10']),
        customer_t1_3=int(data['t1_3']),
        customer_t2_3=int(data['t2_3']),
        customer_t3_3=int(data['t3_3'])
    )

    def get_product():
        response = app.config['products'].PredictProducts(model_data)
        print(response)
        yield MessageToJson(response)

    return Response(get_product(), content_type='application/json')



@app.route('/supplier/', methods=['POST'])
def supplier_get():
    # {"id": 1, "gender": 0, "income": 1, "headOfhousehold": 0, "noOfhousehold": 2, "monthOfresidence": 50,
    #  "t1_30": 332, "t2_30": 23902, "t3_30": 10963, "t1_10": 133, "t2_10": 14473, "t3_10": 3586, "t1_3": 26,
    #  "t2_3": 6824, "t3_3": 1403}
    data = request.get_json()

    model_data = supplier_pb2.SupplierPredictRequest(
        supllier_id=int(data['id'])
    )

    print(model_data)

    def get_supplier():
        response = app.config['supplier'].PredictSuppliers(model_data)
        print('response:', response)
        yield MessageToJson(response)

    return Response(get_supplier(), content_type='application/json')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')